<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
</head>
<body>
    <h1>PRACTICA APUNTADORES</h1>
    <P>Practica de apuntadores de variables en memoria.</P>
    <CODE>
       $edad =26;<BR>
       $Aedad = & $edad;<BR>

       echo "{$edad}\n";<BR>
       unset($edad);<br>
       echo "{$Aedad}\n";<BR>
    </CODE>
    <h2>RESULTADO</h2>
    <h4>{{$Aedad}}</h4>
</body>
</html>