<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>PRACTICA MEMORRIA</h1>
    <P>Practica de cuanta memoria usamos</P>
    <code>public function Memoria(){
        $resultado = '';
        $resultado.= "Memoria en uso". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
        $resultado.= str_repeat("a", 200000)."<br>";
         $resultado.="Memoria en uso". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
         $resultado.='Memoria limite: ' . ini_get('memory_limit') . '<br>';
        return  view('memoria',['resultado'=>$resultado]);
    }
}</code>
<h2>RESULTADO</h2>
<H4>{{$resultado}}</H4>
</body>
</html>